require 'spec_helper'
require "#{File.dirname(__FILE__)}/../../app/job_vacancy/job_vacancy_service"
require "#{File.dirname(__FILE__)}/../../app/job_vacancy/job_vacancy_data_service"

describe JobVacancyService do
  let(:job_vacancy_data_service) { instance_double(JobVacancyDataService) }
  let(:active_offers_response) do
    [{
      'id' => 1,
      'title' => 'Backend Developer',
      'description' => 'Backend Developer',
      'location' => 'Remote'
    }]
  end

  it 'should return active offers' do
    allow(job_vacancy_data_service).to receive(:get_offers).with(status: 'active').and_return(active_offers_response)
    job_vacancy_service = described_class.new(job_vacancy_data_service)
    active_offers = job_vacancy_service.active_offers

    expect(active_offers).to eq(["ID: 1 \nTitle: Backend Developer \nDescription: Backend Developer \nLocation: Remote"])
  end
end
