require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_offers
  stub_request(:get, "#{ENV['JOB_VACANCY_URL']}offers?status=active")
    .with(
      headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: '[{"id": 1,"title": "Backend Developer","description": "Backend Developer","location": "Remote"}]', headers: {})
end

def stub_search(http_code, body_string, search_param = 'ruby')
  stub_request(:get, "#{ENV['JOB_VACANCY_URL']}offers/search?s=#{search_param}")
    .with(
      headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: http_code, body: body_string, headers: {})
end

def stub_similar_to(http_code, body_string, similar_to_param = '1')
  stub_request(:get, "#{ENV['JOB_VACANCY_URL']}offers/similar_to?s=#{similar_to_param}")
    .with(
      headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: http_code, body: body_string, headers: {})
end

describe 'BotClient' do
  before(:each) do
    stub_const('TOKEN', 'fake_token')
  end

  after(:each) do
    app = BotClient.new(TOKEN)

    app.run_once
  end

  it 'should get a /version message and respond with current version' do
    when_i_send_text(TOKEN, '/version')
    then_i_get_text(TOKEN, Version.current)
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    when_i_send_text(TOKEN, '/say_hi Emilio')
    then_i_get_text(TOKEN, 'Hola, Emilio')
  end

  it 'should get a /start message and respond with Hola' do
    when_i_send_text(TOKEN, '/start')
    then_i_get_text(TOKEN, 'Hola, Emilio')
  end

  it 'should get a /stop message and respond with Chau' do
    when_i_send_text(TOKEN, '/stop')
    then_i_get_text(TOKEN, 'Chau, egutter')
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    when_i_send_text(TOKEN, '/tv')
    then_i_get_keyboard_message(TOKEN, 'Quien se queda con el trono?')
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    when_i_send_keyboard_updates(TOKEN, 'Quien se queda con el trono?', '2')
    then_i_get_text(TOKEN, 'A mi también me encantan los dragones!')
  end

  it 'should get an unknown message message and respond with Do not understand' do
    when_i_send_text(TOKEN, '/unknown')
    then_i_get_text(TOKEN, 'Uh? No te entiendo! Me repetis la pregunta?')
  end

  it 'should get a /offers message and respond with a list of offers' do
    when_i_send_text(TOKEN, '/offers')
    stub_offers
    then_i_get_text(TOKEN, "ID: 1 \nTitle: Backend Developer \nDescription: Backend Developer \nLocation: Remote")
    then_i_get_text(TOKEN, 'You can apply to an offer by typing: /apply <ID> <email> <desired remuneration>')
  end

  it 'should get a /search message and respond with a list of matching offers' do
    when_i_send_text(TOKEN, '/search ruby')
    stub_search(200, '[{"id": 1,"title": "Ruby Developer","description": "Developer","location": "Remote"}]')
    then_i_get_text(TOKEN, "ID: 1 \nTitle: Ruby Developer \nDescription: Developer \nLocation: Remote")
  end

  it 'should get a /search message and respond with message of no matching offers' do
    when_i_send_text(TOKEN, '/search ruby')
    stub_search(200, '[]')
    then_i_get_text(TOKEN, 'There are no active offers matching: ruby')
  end

  it 'should get a /search message with no parameter and respond with an error' do
    when_i_send_text(TOKEN, '/search')
    then_i_get_text(TOKEN, 'You must specify a search term: /search <SEARCH TERM>')
  end

  it 'should get a /similar_to message and respond with a list of similar offers' do
    when_i_send_text(TOKEN, '/similar_to 1')
    stub_similar_to(200, '[{"id": 2,"title": "Ruby Developer","description": "Developer","location": "Remote"}]')
    then_i_get_text(TOKEN, "ID: 2 \nTitle: Ruby Developer \nDescription: Developer \nLocation: Remote")
  end

  it 'should get a /similar_to message and respond with message of no similar offers' do
    when_i_send_text(TOKEN, '/similar_to 1')
    stub_similar_to(200, '[]')
    then_i_get_text(TOKEN, 'There are no active offers similar to Ruby Developer.')
  end

  it 'should get a /similar_to message with no parameter and respond with an error' do
    when_i_send_text(TOKEN, '/similar_to')
    then_i_get_text(TOKEN, 'You must specify an offer to compare: /similar_to <OFFER ID>')
  end
end
