class JobOffer
  attr_accessor :id, :title, :location, :description

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
  end
end
