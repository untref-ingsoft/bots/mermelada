class Error
  attr_accessor :message, :code

  def initialize(data = {})
    @message = data[:message]
    @code = data[:code]
  end
end
