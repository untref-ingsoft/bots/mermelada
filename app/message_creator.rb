module MessageCreator
  def self.offer(offer_object)
    "ID: #{offer_object.id} \nTitle: #{offer_object.title} \nDescription: #{offer_object.description} \nLocation: #{offer_object.location}"
  end

  def self.error(error_object)
    error_object.message
  end
end
