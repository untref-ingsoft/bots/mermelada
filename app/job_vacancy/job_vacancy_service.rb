class JobVacancyService
  def initialize(job_vacancy_data_service)
    @job_vacancy_data_service = job_vacancy_data_service
  end

  def active_offers
    active_offers_response = @job_vacancy_data_service.get_offers(status: 'active')
    active_offers_response.map do |offer|
      "ID: #{offer['id']} \nTitle: #{offer['title']} \nDescription: #{offer['description']} \nLocation: #{offer['location']}"
    end
  end

  def search_offers(search_term)
    @job_vacancy_data_service.search_offers(search: search_term)
  end

  def similar_to(similar_to_term)
    @job_vacancy_data_service.similar_to(similar_to: similar_to_term)
  end
end
