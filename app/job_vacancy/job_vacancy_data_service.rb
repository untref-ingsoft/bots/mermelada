require "#{File.dirname(__FILE__)}/../models/job_offer"
require "#{File.dirname(__FILE__)}/../models/error"

class JobVacancyDataService
  def initialize
    @connection = Faraday.new(url: ENV['JOB_VACANCY_URL']) do |faraday|
      faraday.headers['Content-Type'] = 'application/json'
      faraday.headers['Accept'] = 'application/json'
    end
  end

  def get_offers(params = {})
    response = @connection.get('offers') do |req|
      req.params['status'] = params[:status] if params[:status]
    end
    JSON.parse(response.body)
  end

  def search_offers(params = {})
    response = @connection.get('offers/search') do |req|
      req.params['s'] = params[:search] if params[:search]
    end
    format_response(response)
  end

  def similar_to(params = {})
    response = @connection.get('offers/similar_to') do |req|
      req.params['s'] = params[:similar_to] if params[:similar_to]
    end
    format_response(response)
  end

  private

  def format_response(search_response)
    search_json = JSON.parse(search_response.body)
    return Error.new(code: search_json['code'], message: search_json['message']) if search_response.status == 400

    search_json.map do |offer|
      JobOffer.new(id: offer['id'], title: offer['title'], location: offer['location'], description: offer['description'])
    end
  end
end
