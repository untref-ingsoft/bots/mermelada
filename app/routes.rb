require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/message_creator"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/job_vacancy/job_vacancy_service"
require "#{File.dirname(__FILE__)}/job_vacancy/job_vacancy_data_service"

class Routes
  include Routing

  on_message '/offers' do |bot, message|
    job_vacancy_service = JobVacancyService.new(JobVacancyDataService.new)
    active_offers_message_array = job_vacancy_service.active_offers
    active_offers_message_array.each do |active_offer_message|
      bot.api.send_message(chat_id: message.chat.id, text: active_offer_message)
    end
    if active_offers_message_array.empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No active offers')
    else
      bot.api.send_message(chat_id: message.chat.id, text: 'You can apply to an offer by typing: /apply <ID> <email> <desired remuneration>')
    end
  end

  on_message '/search' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify a search term: /search <SEARCH TERM>')
  end

  on_message_pattern %r{/search (?<search_term>.+)} do |bot, message, args|
    search_term = args['search_term']
    job_vacancy_service = JobVacancyService.new(JobVacancyDataService.new)
    search_response = job_vacancy_service.search_offers(search_term)

    if search_response.is_a?(Error)
      bot.api.send_message(chat_id: message.chat.id, text: MessageCreator.error(search_response))
    elsif search_response.empty?
      bot.api.send_message(chat_id: message.chat.id, text: "There are no active offers matching: #{search_term}")
    else
      search_response.each do |search_result|
        bot.api.send_message(chat_id: message.chat.id, text: MessageCreator.offer(search_result))
      end
    end
  end

  on_message '/similar_to' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify an offer to compare: /similar_to <OFFER ID>')
  end

  on_message_pattern %r{/similar_to (?<similar_to_term>.+)} do |bot, message, args|
    similar_to_term = args['similar_to_term']
    job_vacancy_service = JobVacancyService.new(JobVacancyDataService.new)
    search_response = job_vacancy_service.similar_to(similar_to_term)

    if search_response.is_a?(Error)
      bot.api.send_message(chat_id: message.chat.id, text: MessageCreator.error(search_response))
    else
      search_response.each do |search_result|
        bot.api.send_message(chat_id: message.chat.id, text: MessageCreator.offer(search_result))
      end
    end
  end

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message_pattern %r{/say_hi (?<name>.*)} do |bot, message, args|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{args['name']}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/time' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message '/tv' do |bot, message|
    kb = [Tv::Series.all.map do |tv_serie|
      Telegram::Bot::Types::InlineKeyboardButton.new(text: tv_serie.name, callback_data: tv_serie.id.to_s)
    end]
    markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)

    bot.api.send_message(chat_id: message.chat.id, text: 'Quien se queda con el trono?', reply_markup: markup)
  end

  on_message '/busqueda_centro' do |bot, message|
    kb = [
      Telegram::Bot::Types::KeyboardButton.new(text: 'Compartime tu ubicacion', request_location: true)
    ]
    markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)
    bot.api.send_message(chat_id: message.chat.id, text: 'Busqueda por ubicacion', reply_markup: markup)
  end

  on_location_response do |bot, message|
    response = "Ubicacion es Lat:#{message.location.latitude} - Long:#{message.location.longitude}"
    puts response
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_response_to 'Quien se queda con el trono?' do |bot, message|
    response = Tv::Series.handle_response message.data
    bot.api.send_message(chat_id: message.message.chat.id, text: response)
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
